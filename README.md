# BDA TP2

## Run script
```sh 
npm install 
npm start 
```
## View exercices one by one : 
`Change values for cities in : `

```sh
--- Exercice 2 ---
2a: http://localhost:3000/2a/SFO&DEN
2b: http://localhost:3000/2b/LAX
2c: http://localhost:3000/2c/MCO&IAD
2d: http://localhost:3000/2d/ORD
2e: http://localhost:3000/2e/BOS
2f: http://localhost:3000/2f/MCO
2g: http://localhost:3000/2g/LAS
2h: http://localhost:3000/2h/IAH
```

```sh
--- Exercice 3 ---
3a:  http://localhost:3000/3a
3b:  http://localhost:3000/3b
3c:  http://localhost:3000/3c/LAX&MIA
3d:  http://localhost:3000/3d/LAX&MIA
3e:  http://localhost:3000/3c/LAX&MIA
3f1: http://localhost:3000/3f1/LAX&MIA
3f2: http://localhost:3000/3f2/LAX&MIA
```

### Contacts 
---
maria.paramonova@etu.unige.ch







