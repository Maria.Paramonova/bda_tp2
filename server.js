const express = require('express')
const neo4j = require('neo4j-driver')
const server = express()
const port = 3000;
const driver = neo4j.driver('bolt://localhost:7687', neo4j.auth.basic('neo4j', '123'))
const session = driver.session()

server.get('/', function (req, res) {
  res.json({ result: true });
})



server.get('/2a/:origin&:destination', async function (req, res) {
  const origin = req.params.origin;
  const destination = req.params.destination;

  const query = `MATCH (a:Airport)<-[:ORIGIN]-(f:Flight)-[:DESTINATION]->(b:Airport)
  WHERE (a.name = "${origin}" 
  AND b.name = "${destination}") OR (a.name = "${destination}" 
  AND b.name = "${origin}") 
  RETURN COUNT(f)`;

  const result = await session.run(query, {});
  res.json(result.records);
})

server.get('/2b/:name', async function (req, res) {
    const name = req.params.name;
    const query = `MATCH (flight:Flight)-[:ORIGIN |:DESTINATION]->(airport:Airport {name: "${name}"}) 
    RETURN flight.date, flight.airline, flight.duration`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2c/:x&:y', async function (req, res) {
    const x = req.params.x;
    const y = req.params.y;
    const query = `MATCH (origin:Airport)<-[:ORIGIN]-(flight:Flight)-[:DESTINATION]->(dest:Airport)
    WHERE (origin.name="${x}" AND dest.name="${y}") OR (origin.name="${y}" AND dest.name="${x}") WITH DISTINCT flight
    MATCH (flight)<-[:ASSIGN]-(ticket:Ticket)
    RETURN flight.date, flight.airline, ticket.price`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2d/:name', async function (req, res) {
    const name = req.params.name;
    const query = `MATCH (:Airport {name: "${name}"})<-[:ORIGIN]-(flight:Flight)-[:DESTINATION]->(:Airport) 
    RETURN COUNT(flight)`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2e/:x', async function (req, res) {
    const x = req.params.x;
    const query = `MATCH (origin:Airport)<-[:ORIGIN]-(flight:Flight)-[:DESTINATION]->(dest:Airport) 
    WHERE (origin.name = "${x}" OR dest.name="${x}")
    RETURN COUNT(flight)`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2f/:x', async function (req, res) {
    const x = req.params.x;
    const query = `MATCH (flight:Flight)-[:DESTINATION]->(:Airport {name: "${x}"}) 
    WITH DISTINCT flight
    MATCH (flight)<-[:ASSIGN]-(ticket:Ticket)
    RETURN AVG(ticket.price)`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2g/:x', async function (req, res) {
    const x = req.params.x;
    const query = `MATCH (flight:Flight)-[:ORIGIN]->(:Airport {name: "${x}"}) 
    WITH DISTINCT flight
    MATCH (flight)<-[:ASSIGN]-(ticket:Ticket)
    RETURN AVG(ticket.price)`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/2h/:x', async function (req, res) {
    const x = req.params.x;
    const query = `MATCH (origin:Airport)<-[:ORIGIN]-(flight:Flight)-[:DESTINATION]->(dest:Airport)
    WHERE (origin.name="${x}") OR ( dest.name="${x}") WITH DISTINCT flight
    MATCH (flight)<-[:ASSIGN]-(ticket:Ticket)
    RETURN MAX(ticket.price), MIN(ticket.price)`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3a', async function (req, res) {
    const x = req.params.x;
    const query = `MATCH (origin:Airport)<-[:ORIGIN]-(f:Flight)-[:DESTINATION]->(d:Airport) 
    CREATE (origin)-[path:PATH { name: origin.name + ' -> ' + d.name, duration: f.duration, distance: f.distance }]->(d)
    RETURN path.name, path.duration, path.distance`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3b', async function (req, res) {
    const query = `MATCH (origin:Airport)<-[:ORIGIN]-(flight:Flight)-[:DESTINATION]->(destination:Airport)
    WITH origin, flight, destination
   MATCH (origin)-[path:PATH]->(destination)
   WITH origin, flight, path, destination
   MATCH (flight)<-[:ASSIGN]-(ticket:Ticket)
   WITH origin, flight, path, destination, MIN(ticket.price) AS lowestPrice SET path.lowestPrice = lowestPrice
   RETURN DISTINCT path.name, path.lowestPrice`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3c/:x&:y', async function (req, res) {
    const x = req.params.x;
    const y = req.params.y;
    const query = `MATCH paths = (:Airport {name: "${x}"})-[:PATH*1..3]->(:Airport {name : "${y}"}) 
    WITH reduce(path = [], p in relationships(paths) | path + p.name) AS path 
    RETURN DISTINCT path`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3d/:x&:y', async function (req, res) {
    const x = req.params.x;
    const y = req.params.y;
    const query = `MATCH paths = (:Airport {name: "${x}"})-[:PATH*1..4]->(:Airport {name : "${y}"}) 
    WITH reduce(path = [], p in relationships(paths) | path + p.name) AS path RETURN
    DISTINCT path`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3f1/:x&:y', async function (req, res) {
    const x = req.params.x;
    const y = req.params.y;
    const query = `MATCH (origin:Airport {name: "${x}"}), (d:Airport {name: "${y}"})
    CALL algo.shortestPath.stream(origin, d, 'distance', {relationshipQuery:'PATH'}) YIELD nodeId, cost
    RETURN algo.getNodeById(nodeId).name AS destination, cost AS distance`;
    const result = await session.run(query, {});
    res.json(result.records);
})

server.get('/3f2/:x&:y', async function (req, res) {
    const x = req.params.x;
    const y = req.params.y;
    const query = `MATCH (origin:Airport {name: "${x}"}), (d:Airport {name: "${y}"})
    CALL algo.shortestPath.stream(origin, d, 'duration', {relationshipQuery:'PATH'}) YIELD nodeId, cost
    RETURN algo.getNodeById(nodeId).name AS destination, cost AS time`;
    const result = await session.run(query, {});
    res.json(result.records);
})


server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});
